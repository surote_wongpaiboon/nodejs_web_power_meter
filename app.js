var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mqtt = require('mqtt');
var MongoClient = require('mongodb').MongoClient;
var mqttbroker = 'localhost';
var mqttport = 1883;

var mqttclient = mqtt.createClient(mqttport, mqttbroker);


app.get('/', function(req, res) {
    res.sendfile('index.html');
});

app.set('view engine', 'jade');

app.get('/ajax', function(req, res) {
    console.log(req.query.name)
    console.log(req.query.all)
    MongoClient.connect("mongodb://localhost:27017/testnodeX", function(err, db) {
        if (!err) {
            console.log("We are connected");
        }
        var collection = db.collection(req.query.topic); //from power or power2 jade

        collection.find({

        }).toArray(function(err, result) {
            if (err) {
                console.log(err);
            } else if (result.length) {
                var wh = []
                var time = []
                var alldate = []
                    //console.log(String(result[0].time).split(' ')[4].split(':')[0])
                var old_min = String(result[0].time).split(' ')[4].split(':')[1]
                var old_date = String(result[0].time.getDate())
                var old_hour = String(result[0].time.getHours())
                console.log(result[0].time.getHours())
                console.log(result[0].time.getMinutes())
                for (i = 0; i < result.length; i++) {
                    if (req.query.all == String(result[i].time.getDate())+" "+String(result[i].time.getMonth()+1)+" "+String(result[i].time.getFullYear())) {

                        //for time with in 10 or 20 or 50 mins if LT now then push to time
                        if ((old_hour * 60) + Number(old_min) + Number(req.query.name) <= (result[i].time.getHours() * 60) + Number(String(result[i].time.getMinutes()))) {
                            wh.push(result[i].wh)
                                //    console.log(String(result[i].time).split(' ')[4].replace(/:/g,''));
                            time.push(String(result[i].time.getHours()) + ":" + String(result[i].time.getMinutes())) //[day month date year time xxx]
                            old_min = String(result[i].time.getMinutes())

                            //    console.log(same_min);
                        }
                        if (old_hour != String(result[i].time.getHours())) //set hour and min for next hour
                        {
                            old_hour = String(result[i].time.getHours())
                            old_min = String(result[i].time.getMinutes())
                        }
                        if (old_date != String(result[i].time.getDate())) {
                            alldate.push(String(result[i].time.getDate()))
                            old_date = String(result[i].time.getDate())
                        }
                    }
                }
                res.send({
                    title: 'Energy from Database',
                    message: wh,
                    time: time,
                    reqx: req.query.name, //debug
                    showdate: alldate,
                    date: req.query.all
                });
            } else {
                console.log('No document(s) found with defined "find" criteria!');
            }
        });

    });
});
app.get('/test', function(req, res) {
    MongoClient.connect("mongodb://localhost:27017/testnodeX", function(err, db) {
        if (!err) {
            console.log("We are connected");
        }
        var collection = db.collection('power2');

        collection.find({

        }).toArray(function(err, result) {
            if (err) {
                console.log(err);
            } else if (result.length) {
                var wh = []
                var time = []
                var alldate = []
                var same_min = String(result[0].time).split(' ')[4].split(':')[1]
                var same_date = String(result[0].time.getDate()) + " " + String(result[0].time.getMonth() + 1) + " " + String(result[0].time.getFullYear())
                alldate.push(same_date)
                for (i = 0; i < result.length; i++) {
                    if (Number(same_min) != Number(String(result[i].time).split(' ')[4].split(':')[1])) {
                        wh.push(result[i].wh)
                        time.push(String(result[i].time).split(' ')[4].replace(/:/g, '')) //[day month date year time xxx]
                        same_min = String(result[i].time).split(' ')[4].split(':')[1]

                    }

                    if (same_date != String(result[i].time.getDate()) + " " + String(result[i].time.getMonth() + 1) + " " + String(result[i].time.getFullYear())) {
                        var new_date = String(result[i].time.getDate()) + " " + String(result[i].time.getMonth() + 1) + " " + String(result[i].time.getFullYear())
                        alldate.push(new_date)
                        same_date = new_date
                    }

                }
                console.log(alldate)
                res.render('index', {
                    title: 'Energy from Database',
                    message: wh,
                    time: time,
                    showdate: alldate
                });
            } else {
                console.log('No document(s) found with defined "find" criteria!');
            }
        });

    });

});

var mqtt_sub = function(topics) {
    console.log(topics);
    mqttclient.subscribe(topics, function() {
        mqttclient.on('message', function(topic, payload) {

            console.log(topic.split("/")[4]) //power or power2
                //    console.log(payload)
                //    console.log((parseInt(payload[2], 16) << 8) + parseInt(payload[3], 16)) //energy that come from sensor must shifted before use
                // Connect to the db
            MongoClient.connect("mongodb://localhost:27017/testnodeX", function(err, db) {
                if (!err) {
                    console.log("We are connected");
                } else {
                    return err;
                }
                console.log(db)
                var collection = db.collection(topic.split("/")[4]); //create collection with nodename

                var power = {
                    nodename: topic,
                    time: new Date(),
                    wh: (parseInt(payload[2], 16) << 8) + parseInt(payload[3], 16)
                };

                collection.insert(power, function(err, result) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
                    }
                });

            });

            io.sockets.emit('mqtt' + ':' + topic, {
                'topic': topic,
                'payload': payload,
                'bat': (parseInt(payload[2], 16) << 8) + parseInt(payload[3], 16)
            });

        });
    });
}



io.on('connection', function(socket) {

    console.log("connected server");

    /*    socket.on('subscribe', function(data) {
            console.log(data);
            mqttclient.subscribe(data.topic, function() {
                mqttclient.on('message', function(topic, payload) {

                    console.log(topic.split("/")[4]) //power or power2
                        //    console.log(payload)
                        //    console.log((parseInt(payload[2], 16) << 8) + parseInt(payload[3], 16)) //energy that come from sensor must shifted before use
                        // Connect to the db
                    MongoClient.connect("mongodb://localhost:27017/testnodeX", function(err, db) {
                        if (!err) {
                            console.log("We are connected");
                        }
                        console.log(db)
                        var collection = db.collection(topic.split("/")[4]); //create collection with nodename

                        var power = {
                            nodename: topic,
                            time: new Date(),
                            wh: (parseInt(payload[2], 16) << 8) + parseInt(payload[3], 16)
                        };

                        collection.insert(power, function(err, result) {
                            if (err) {
                                console.log(err);
                            } else {
                                console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
                            }
                        });

                    });

                    io.sockets.emit('mqtt' + ':' + topic, {
                        'topic': topic,
                        'payload': payload,
                        'bat': (parseInt(payload[2], 16) << 8) + parseInt(payload[3], 16)
                    });

                });
            });
        });*/




});
mqtt_sub('iot-2/evt/status/fmt/power')

mqtt_sub('iot-2/evt/status/fmt/power2')
http.listen(3003, function() {
    console.log('listening on *:3003');
});
